#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Response {

	private String outpuMessage;

	@JsonCreator
	public Response(@JsonProperty("outpuMessage") String outpuMessage) {
		super();
		this.outpuMessage = outpuMessage;
	}

	public String getOutpuMessage() {
		return outpuMessage;
	}

	public void setOutpuMessage(String outpuMessage) {
		this.outpuMessage = outpuMessage;
	}

}
