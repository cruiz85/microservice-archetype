#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ${package}.model.Request;
import ${package}.model.Response;
import ${package}.service.AccountSummaryService;

@RestController
@RequestMapping("/accountssumary")
public class AccountSummaryController implements AccountSummaryService {

	public Response test(@RequestBody Request inputObject) {
		return new Response(inputObject.getMessage() + " hello");
	}

}
